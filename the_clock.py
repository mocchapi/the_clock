#!/usr/bin/python3
from os import path
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication

class mainWidget(QWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        loadUi(path.join(path.dirname(path.realpath(__file__)),'the_clock.ui'),self)

def main(args=[]):
    app = QApplication(args)
    widget = mainWidget()
    window = QMainWindow()
    window.setCentralWidget(widget)
    widget.b1.clicked.connect(window.close)
    widget.b1.setFocus()
    widget.b2.clicked.connect(window.close)
    window.show()
    app.exec()

if __name__ == '__main__':
    main()